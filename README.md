### Ultimo 2.3.1

作为奠定规矩，往后整理制作 theme 时，modman 用不上的目录和文件统一放到 theme 同名目录下

generate-modman 生成的 modman 文件不尽如人意，须手动修改
app/design/frontend/ultimo 为 app/design/frontend/ultimo/default
skin/frontend/ultimo 为 skin/frontend/ultimo/default
这样才方便添加 sub-theme 嘛

patch/ 下的东西按具体情况采用